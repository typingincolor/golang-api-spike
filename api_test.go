package main

import (
	"github.com/jmcvetta/napping"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

type JsonResult struct {
	Apple int `json:"apple"`
	Pear  int `json:"pear"`
}

var _ = Describe("Api", func() {
	It("should work", func() {
		json := JsonResult{}
		resp, _ := napping.Get("http://localhost:3000", nil, &json, nil)
		Expect(resp.Status()).To(Equal(200))
		Expect(json.Apple).To(Equal(5))
		Expect(json.Pear).To(Equal(7))
	})
})
