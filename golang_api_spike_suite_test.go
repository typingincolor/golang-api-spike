package main

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"testing"
)

func TestGolangApiSpike(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "GolangApiSpike Suite")
}
