# Simple API written in golang

## Installing dependencies

I've used [goop][1] to manage the dependencies, it is installed as follows:

    go get github.com/nitrous-io/goop

and the dependencies installed using

    goop install

## Running to tests

The API must be running, it is started using

    goop go run api.go

The tests are run using

    goop go test

  [1]: https://github.com/nitrous-io/goop
