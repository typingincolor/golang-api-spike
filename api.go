package main

import (
	"encoding/json"
	"fmt"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
	"net/http"
)

func HomeHandler(w http.ResponseWriter, req *http.Request) {
	result := map[string]int{"apple": 5, "pear": 7}
	jsonResult, _ := json.Marshal(result)
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, string(jsonResult))
}

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/", HomeHandler)

	n := negroni.Classic()
	n.UseHandler(router)
	n.Run(":3000")
}
